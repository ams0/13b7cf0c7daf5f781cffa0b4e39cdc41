#!/bin/bash

#kubectl aliases
curl https://raw.githubusercontent.com/ahmetb/kubectl-alias/master/.kubectl_aliases -o ~/.kubectl_aliases ; source ~/.kubectl_aliases
echo "source ~/.kubectl_aliases" >> ~/.bashrc
source ~/.kubectl_aliases

#kube-ps1
wget https://raw.githubusercontent.com/jonmosco/kube-ps1/refs/heads/master/kube-ps1.sh -O $HOME/.kube-ps1.sh
source "$HOME/.kube-ps1.sh"
PS1='$(kube_ps1)'$PS1

# krew
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
echo 'export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"' >> ~/.bashrc

kubectl krew install ctx node-admin stern tail tree resource-capacity neat

grep -qxF '[core]'  ~/.azure/config || echo -e '[core]\noutput = table' >>  ~/.azure/config 
#export CLUSTER_RG=rg-test-alessandro ; export CLUSTER_NAME=infra
#az aks get-credentials --resource-group $CLUSTER_RG -n  $CLUSTER_NAME  --overwrite-existing -a
wget https://github.com/derailed/k9s/releases/download/v0.32.5/k9s_Linux_amd64.tar.gz  -O - | tar -xz

mkdir -p ~/.local/bin ; mv k9s ~/.local/bin

alias stern="kubectl stern"
az extension add --name aks-preview --allow-preview true
az configure -d location=westeurope
az account set --subscription sub-dev-eu-kubehub
export CLUSTER_RG=rg-alessandro ; export CLUSTER_NAME=infra
export NODE_SIZE=Standard_D2s_v3
export BASE_NODE_COUNT=3
export AKS_VERSION=1.31.1
export LOCATION=westeurope

alias goaks='az aks create --ssh-access disabled --resource-group $CLUSTER_RG --name $CLUSTER_NAME --node-count $BASE_NODE_COUNT --min-count 1 --max-count 5 --node-vm-size $NODE_SIZE --generate-ssh-keys     --location $LOCATION     --kubernetes-version $AKS_VERSION     --auto-upgrade-channel rapid     --enable-aad     --enable-image-cleaner     --network-plugin azure     --network-plugin-mode overlay     --pod-cidr 192.168.0.0/16     --network-dataplane cilium     --node-os-upgrade-channel NodeImage     --os-sku AzureLinux --enable-cluster-autoscaler'
